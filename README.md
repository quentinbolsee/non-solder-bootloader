# Non-solder bootloader

This doohickey can be used to flash a bootloader or a binary onto a SAMD21E without soldering it. This is useful when the final board for the SAMD21E is too small to afford having a swd connector.

![](img/use.jpg)

This is still experimental and not 100% reliable, but the board simply consists of copper traces onto which the QFP32 chip is pressed with a 3D printed jig. The 3D printed part is compliant and presses on each leg adequately.

This board features a SAMD11 as a permanent USB-to-swd programmer so that flashing is as simple as plugging the board in and launching edbg or the [Arduino core](https://gitlab.cba.mit.edu/quentinbolsee/Arduino-SAM-tutorial/).

## Design

### Schematic

The schematic is the bare minimum, with a SAMD11 as a USB-to-swd programmer connected to the target SAMD21. The fact that the SAMD21 will not be soldered is not visible in the schematic, or the board design for that matter.

![](img/schematic.png)

An LED is connected to pin PA00 of the target SAMD21, in case you want to flash an LED blinking code onto it to make sure it's functional.

### Board layout

The board design is split in two: the USB and D11 programmer on top, and the non-solder area on the bottom. Note that only a few pins are connected on the target, the bare minimum plus an LED for testing.

![](img/board.png)

M3 holes allow for future improvement on the pressing method, the spacing is:

- horizontal: 1.2in
- vertical: 0.8in

The png images for fabrication can be found [here](design/png) and the kicad files [here](design/kicad).

### CAD

The press has a lip on its bottom side to apply load directly on the legs of the QFP32 target. There is still some improvement needed there but it works for now.

![](img/press.png)

The 3D files can be found [here](design/cad).

## Making

The board can be milled with a 1/64" flat end mill:

![](img/milling.jpg)

After removing all of the extra copper with a 1/32" end mill (for speed), the board looks very simple:

![](img/milling_done.jpg)

After flashing the [free-dap](https://github.com/ataradov/free-dap) on the SAMD11, you can attach a SAMD21 target with M3 screws and launch a flashing like you normally would when using a programmer.

![](img/use.jpg)
